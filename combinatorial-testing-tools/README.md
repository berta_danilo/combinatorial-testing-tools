# Combinatorial Testing Tools

REVERSE COMBINATORIAL TEST PROBLEM:Given a Test Set for which you do not know the method of generation (if any), calculate what percentage of nwise-coverage the Test Set ensures, with nwise between 1 and the number variables in the Test Set.